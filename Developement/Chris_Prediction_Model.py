from alpha_vantage.timeseries import TimeSeries
from alpha_vantage.techindicators import TechIndicators
from scipy.ndimage.interpolation import shift
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import StackingRegressor
import math
import numpy as np

import time
# import matplotlib.pyplot as plt


def Kelly_Model(ticker,predict, actual, money):
    actual = np.squeeze(actual)
    predict_right= 0
    total_trades = 0
    gains = 0.0
    losses = 0.0
    stocks = 0

    for i in range(0, len(actual)-1):

        diff = predict[i+1]-actual[i]
        val = (0.002*actual[i])
        if diff > .002*actual[i] and stocks < 1:
            stocks = 1
            total_trades += 1
            last_trade = actual[i]

        elif diff < (-0.002*actual[i]) and stocks > 0:
            stocks =0
            if actual[i]>last_trade:
                predict_right+=1
                gains += float(actual[i] - last_trade)
            else:
                losses += abs(float(actual[i] - last_trade))





    W = float(predict_right/total_trades)
    R = float(gains/losses)
    K = W - ((1- W) / R)

    print("Gains ", gains)
    print("Losses ", losses)
    print("Win / Total Trades:", predict_right,total_trades)
    print("Revenue Ratio:",R)
    print("Win/Loss ratio: ",W)
    print("Kelly: ", K)
    print("Difference: ",predict[-1]-actual[-2])

    # if K <0 :
    #     print('Negative Kelly')
    #     return 'Negative Kelly'
    # if(predict[-1] - actual[-2] > 0.002*actual[-2]):
    #     print(ticker+" Buy $",money*K)
    #     return money*K
    # else:
    #     print(ticker+" Sell" )
    #     return 'Sell'




def plotPrediction(model,predicted,actual):
    import matplotlib.pyplot as plt
    plt.plot(actual, color='red', label='Actual Stock Price')
    plt.plot(predicted, color='blue', label='Predicted Stock Price')

    plt.title(model+' Stock Price Prediction')
    plt.xlabel('Time')
    plt.ylabel('Stock Price')
    plt.legend()
    plt.show()


def saveDailyData(ticker,size):
    from alpha_vantage.timeseries import TimeSeries
    from alpha_vantage.techindicators import TechIndicators
    # Your key here
    key = 'NR0TTJWDRGF1P8'
    ts = TimeSeries(key)
    ti = TechIndicators(key)
    data, meta = ts.get_daily(ticker, size)
    rsi, meta = ti.get_rsi(ticker,interval='daily',time_period='20',series_type='close')
    stoch, meta = ti.get_stoch(ticker, interval='daily')
    ema, meta = ti.get_ema(ticker, interval='daily',time_period="26")
    macd, meta = ti.get_macd(ticker, interval='daily')

    print(next(iter(data.items())))
    merged = []
    for day in data:
        combined = list(data[day].values())+ \
        list(rsi[day].values() if day in rsi else ['0']) + \
        list(ema[day].values() if day in ema else ['0']) + \
        list(stoch[day].values() if day in stoch else ['0','0']) + \
        list(macd[day].values() if day in macd else ['0', '0','0'])

        merged.append(combined)

    merged = np.asarray(merged,dtype=np.float32)

    # Reverse data set
    merged = merged[::-1]
    np.savetxt("Daily_"+ticker.replace('TSX:','')+".csv", merged, delimiter=",")

def train_Arima(data, backtest):
    from statsmodels.tsa.arima_model import ARIMA
    #print("Train ARIMA Model")
    close_index = 3
    # split and reshape X and Y data
    X = data[:, close_index]
    X = np.reshape(X, (-1, 1))
    X_train = X[:-backtest]
    X_test = X[-backtest:]

    history = [x for x in X_train]
    predictions = list()

    for t in range(len(X_test)):
        model = ARIMA(history, order=(4, 1, 0))
        model_fit = model.fit(disp=0, transparams=False)
        output = model_fit.forecast()
        yhat = output[0]
        predictions.append(yhat)
        obs = X_test[t]
        history.append(obs)
        #print('predicted=%f, expected=%f' % (yhat, obs))
    error = math.sqrt(mean_squared_error(X_test, predictions))
    print('Test MSE: %.3f' % error)
    # pyplot.plot(X_test, color='red')
    # pyplot.plot(predictions, color='blue')
    # pyplot.title("ARIMA")
    # pyplot.show()
    print("Arima Latest Prediction:")
    print(predictions[-1])
    return predictions

def trainSVRModel(data,forcast=1,backtest=365):
    # https://towardsdatascience.com/walking-through-support-vector-regression-and-lstms-with-stock-price-prediction-45e11b620650
    from sklearn.svm import SVR

    close_index = 3


    # split and reshape X and Y data
    X, Y = data[:,close_index], data[:,close_index]
    Y = shift(Y, -forcast, cval=np.NaN)
    X = np.reshape(X, (-1, 1))

    # Test and Train
    # val = math.floor(.7 * data.shape[0])
    X_train = X[:-backtest]
    Y_train = Y[:-backtest]
    X_test = X[-backtest:]
    Y_test = Y[-backtest:]

    svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)
    svr_rbf.fit(X_train, Y_train)
    predictions = np.reshape(svr_rbf.predict(X_test),(-1,1))

    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(predictions[:-forcast], Y_test[:-forcast]))
    print('SVR RMSE: %.3f' % rmse)
    # plotPrediction("SVR",predictions,Y_test)
    return (predictions,np.reshape(Y_test,(-1,1)))


def predictLSTM(ticker,data,time_steps,forcast):
    from keras.models import load_model
    from sklearn.preprocessing import MinMaxScaler

    model = load_model(ticker+'_LSTM_Model.h5')

    close_index = 3
    # Scale Data
    X_scaler = MinMaxScaler(feature_range=(0, 1))
    Y_scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = X_scaler.fit_transform(data)
    Y_scaler.min_, Y_scaler.scale_ = X_scaler.min_[close_index], X_scaler.scale_[close_index]


    # Prepare data for LSTM
    X = []
    Y = []
    for i in range(time_steps, len(scaled_data)):
        X.append(scaled_data[i - time_steps:i, :])
        Y.append(scaled_data[i,close_index])

    X,Y = np.array(X),np.array(Y)
    Y = shift(Y, -forcast, cval=np.NaN)

    predictions = model.predict(X)
    predictions = Y_scaler.inverse_transform(predictions)
    Y = np.reshape(Y, (-1, 1))
    Y = Y_scaler.inverse_transform(Y)

    print("LSTM Latest prediction")
    print(predictions[-1])

    return predictions,Y

def trainfeedForward(ticker, data, forcast=1, backtest=365):
    from sklearn.preprocessing import MinMaxScaler
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import Dropout

    close_index = 3
    epochs = 2
    batch = 5

    # Scale Data
    X_scaler = MinMaxScaler(feature_range=(0, 1))
    Y_scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = X_scaler.fit_transform(data)
    Y_scaler.min_, Y_scaler.scale_ = X_scaler.min_[close_index], X_scaler.scale_[close_index]


    # split and reshape X and Y data
    X, Y = scaled_data[:, :], scaled_data[:, close_index]
    Y = shift(Y, -forcast, cval=np.NaN)


    # Test and Train
    X_train = X[:-backtest]
    Y_train = Y[:-backtest]
    X_test = X[-backtest:]
    Y_test = Y[-backtest:]


    layer1Size = 24
    layer2Size = 24
    layer3Size = 12
    layer4Size = 6
    layer5Size = 3

    # dataset size
    inpDim = X_train.shape[1]

    # create model
    model = Sequential()
    model.add(Dense(layer1Size, input_dim=inpDim, activation='relu'))
    model.add(Dense(layer2Size, activation='relu'))
    model.add(Dropout(0.1))
    model.add(Dense(layer3Size, activation='relu'))
    model.add(Dense(layer4Size, activation='relu'))

    model.add(Dense(layer5Size, activation='relu'))
    model.add(Dense(1, activation='linear'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae'])

    # Fit the model
    history = model.fit(X_train, Y_train, epochs=epochs, batch_size=batch)
    model.save(ticker +' FeedForward_Model.h5')

    # Inverse transform and reshape
    predictions = model.predict(X_test)
    predictions = Y_scaler.inverse_transform(predictions)
    Y_test = np.reshape(Y_test, (-1, 1))
    Y_test = Y_scaler.inverse_transform(Y_test)

    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(predictions[:-forcast], Y_test[:-forcast]))
    print('FeedForward RMSE: %.3f' % rmse)
    print("FeedForward Latest prediction")
    print(predictions[-1])

    # plot prediction
    # plotPrediction("FeedForward", predictions, Y_test)
    return predictions, Y_test

def trainLSTMModel(ticker,data,time_steps=14,forcast=1,backtest=365):
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import LSTM
    from keras.layers import Dropout
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.svm import SVR
    close_index = 3
    epochs =2
    batch = 20

    # Scale Data
    X_scaler = MinMaxScaler(feature_range=(0, 1))
    Y_scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = X_scaler.fit_transform(data)
    Y_scaler.min_,Y_scaler.scale_ = X_scaler.min_[close_index],X_scaler.scale_[close_index]

    # Prepare data for LSTM
    X = []
    Y = []
    for i in range(time_steps, len(scaled_data)):
        X.append(scaled_data[i-time_steps:i,:])
        Y.append(scaled_data[i, close_index])

    X, Y = np.array(X), np.array(Y)
    Y = shift(Y, -forcast, cval=np.NaN)


    X_train = X[:-backtest]
    Y_train = Y[:-backtest]
    X_test = X[-backtest:]
    Y_test = Y[-backtest:]


    regressor = Sequential()

    regressor.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1],X_train.shape[2])))
    regressor.add(Dropout(0.2))

    regressor.add(LSTM(units=50, return_sequences=True))
    regressor.add(Dropout(0.2))

    regressor.add(LSTM(units=50, return_sequences=True))
    regressor.add(Dropout(0.2))

    regressor.add(LSTM(units=50))
    regressor.add(Dropout(0.2))

    regressor.add(Dense(units=1))

    regressor.compile(optimizer='adam', loss='mean_squared_error',metrics=['mae'])

    regressor.fit(X_train, Y_train, epochs=epochs, batch_size=batch)
    regressor.save(ticker+'_LSTM_Model.h5')
    print("Training Finished ")

    # Inverse transform and reshape
    predictions = regressor.predict(X_test)
    predictions = Y_scaler.inverse_transform(predictions)
    Y_test = np.reshape(Y_test,(-1,1))
    Y_test = Y_scaler.inverse_transform(Y_test)

    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(predictions[:-forcast], Y_test[:-forcast]))
    print('LSTM RMSE: %.3f' % rmse)
    print("LSTM Latest prediction")
    print(predictions[-1])

    # plot prediction
    # plotPrediction("LSTM",predictions,Y_test)
    return predictions,Y_test

def stackingRegressor(x,y,backtest=30):
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import Dropout

    # backtest = int(0.70*x.shape[0])

    X_train = x[:backtest]
    Y_train = y[:backtest]
    X_dev = x[backtest:-1] #Test up until the second last because the last is tomorrows prediction
    Y_dev = y[backtest:-1]

    # hyperparameters
    epochs = 10
    batch_size = 1
    layer1Size = 10
    layer2Size = 10
    layer3Size = 10
    layer4Size = 5
    layer5Size = 3

    # dataset size
    inpDim = X_train.shape[1]

    # create model
    model = Sequential()
    model.add(Dense(layer1Size, input_dim=inpDim, activation='relu'))
    model.add(Dense(layer2Size, activation='relu'))
    model.add(Dense(layer3Size, activation='relu'))
    model.add(Dense(layer4Size, activation='relu'))
    model.add(Dense(layer5Size, activation='relu'))
    model.add(Dense(1, activation='linear'))

    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mae'])

    # Fit the model
    history = model.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size)

    #Evaluate the model
    predictions = model.predict(X_dev)

    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(predictions[:-1], Y_dev[:-1]))
    print('Stacked RMSE: %.3f' % rmse)

    print("Stacked Latest prediction")
    print(predictions[-1])

    plotPrediction("Stacked",predictions[:-1], Y_dev[:-1])

    return predictions,Y_dev




def main(tickers,money):
    tickers = ['TSX:K', 'TSX:MFC', 'TSX:ABT','TSX:AQN','TSX:HSE','TSX:ALA']
    money = 51.4
    # Main
    train_model = False
    results = []
    # Saving TSX Stock Data
    for ticker in tickers:
        print("----- "+ticker+ " Data" +"-----")
        api_ticker = ticker
        saveDailyData(api_ticker,'full')
        ticker = api_ticker.replace('TSX:','')

        #Open get data from file you want to model
        filename = "Daily_"+ticker+".csv"
        data = np.genfromtxt(filename, delimiter=',')

        # Number of days to backtest
        backtest = 60
        time_steps = 60

        #Training the LSTM
        if train_model:
            lstm_1,test_data = trainLSTMModel(ticker,data,time_steps=time_steps,forcast=1,backtest=backtest)
        else:
            lstm_1,test_data = predictLSTM(ticker,data[-backtest-time_steps:],time_steps,forcast=1)
        arma = train_Arima(data,backtest)

        # # Other models
        # svr_1, test_data = trainSVRModel(data,forcast=1,backtest=backtest)

        #
        # # Combine models
        average = np.average(np.concatenate((lstm_1,arma),axis=1),axis=1)

        # Invest Decision Model
        bet = Kelly_Model(ticker,average,test_data,money=money)
        results.append(bet)
        print("-----------------")
        time.sleep(30)


    for i in range(len(results)):
      print(tickers[i] + " " + str(results[i]))





train_model = True
api_ticker = 'TSX:BCE'
saveDailyData(api_ticker,'full')
ticker = api_ticker.replace('TSX:','')

#Open get data from file you want to model
filename = "Daily_"+ticker+".csv"
data = np.genfromtxt(filename, delimiter=',')

# Number of days to backtest
backtest = 252 * 4
time_steps = 62




#Training Feed forward
ff, test_data = trainfeedForward(ticker,data,forcast=1,backtest=backtest)
# arma = train_Arima(data,backtest)

# Other models
svr_1, test_data = trainSVRModel(data,forcast=1,backtest=backtest)

# Training the LSTM
if train_model:
    lstm_1,test_data = trainLSTMModel(ticker,data,time_steps=time_steps,forcast=1,backtest=backtest)
else:
    lstm_1,test_data = predictLSTM(ticker,data[-backtest-time_steps:],time_steps,forcast=1)

# Combine models
combined = np.concatenate((lstm_1,svr_1,ff),axis=1)
stacked,stacked_test_data = stackingRegressor(combined,test_data,backtest=252*2)

# Invest Decision Model
bet = Kelly_Model(ticker,stacked,stacked_test_data,money=50)

