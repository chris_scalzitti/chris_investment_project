from pandas import read_csv
from pandas import datetime
from matplotlib import pyplot
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from statsmodels.graphics.tsaplots import plot_acf,plot_pacf
import numpy as np
#
# series = read_csv('Daily_K.csv', header=None, squeeze=True)
# print(series)
# A = series[3].values
# size = int((len(A)-1500)* 0.99 + 1500)
# print(size)
# train, test = A[1500:size], A[size:len(A)]
# history = [x for x in train]
# predictions = list()
# traindiff1 = series.diff().fillna(series)
# traindiff2 = traindiff1.diff().fillna(traindiff1)
# #
# # pyplot.plot(series[3].values)
# # pyplot.show()
# #
# # pyplot.plot(traindiff1[3].values)
# # pyplot.show()
# #
# # pyplot.plot(traindiff2[3].values)
# # pyplot.show()
#
# plot_acf(traindiff1[3].values)
# pyplot.show()
#
# plot_pacf(traindiff1[3].values)
# pyplot.show()
#
# # Arma_model = ARIMA(series[2].values, order=(4,1,1)).fit(transparams=False)
# # print(Arma_model.summary())
#
# for t in range(len(test)):
#     model = ARIMA(history, order=(4, 1, 0))
#     model_fit = model.fit(disp=0,transparams=False)
#     output = model_fit.forecast()
#     yhat = output[0]
#     predictions.append(yhat)
#     obs = test[t]
#     history.append(obs)
#     print('predicted=%f, expected=%f' % (yhat, obs))
# error = mean_squared_error(test, predictions)
# print('Test MSE: %.3f' % error)
# # plot
# pyplot.plot(test)
# pyplot.plot(predictions, color='red')
# pyplot.show()
# #
# # train, test = traindiff1[1500:size], traindiff1[size:len(A)]
# # history = [x for x in train]
# # predictions = list()
# #
# # for t in range(len(test)):
# #     model = ARIMA(history, order=(1, 0, 0))
# #     model_fit = model.fit(disp=0, transparams=False)
# #     output = model_fit.forecast()
# #     yhat = output[0]
# #     predictions.append(yhat)
# #     obs = test[t]
# #     history.append(obs)
# #     # print('predicted=%f, expected=%f' % (yhat, obs))
# # error = mean_squared_error(test, predictions)
# # print('Test MSE: %.3f' % error)
# # # plot
# # pyplot.plot(test)
# # pyplot.plot(predictions, color='red')
# # pyplot.show()

def train_Arima(data, backtest):
    print("Train ARIMA Model")
    close_index = 3
    # split and reshape X and Y data
    X = data[:, close_index]
    X = np.reshape(X, (-1, 1))
    X_train = X[:-backtest]
    X_test = X[-backtest:]

    history = [x for x in X_train]
    predictions = list()

    for t in range(len(X_test)):
        model = ARIMA(history, order=(4, 1, 0))
        model_fit = model.fit(disp=0, transparams=False)
        output = model_fit.forecast()
        yhat = output[0]
        predictions.append(yhat)
        obs = X_test[t]
        history.append(obs)
        #print('predicted=%f, expected=%f' % (yhat, obs))
    error = mean_squared_error(X_test, predictions)
    print('Test MSE: %.3f' % error)
    pyplot.plot(X_test, color='red')
    pyplot.plot(predictions, color='blue')
    pyplot.title("ARIMA")
    pyplot.show()
    return predictions
def line_predict (data,backtest):
    print("Train Linear Model ")
    maximum_val=.2
    close_index = 3
    # split and reshape X and Y data
    X = data[:, close_index]
    X = np.reshape(X, (-1, 1))
    X_train = X[:-backtest]
    X_test = X[-backtest:]
    predict = [X_train[-1]]
    for t in range(1,len(X_test)):
        slope = (X_test[t] - X_test[t-1])/(t-(t-1))
        b = X_test[t]-slope*t
        value = slope * (t+1) +b
        if abs(value - X_test[t]) > maximum_val:
            value  =  X_test[t] -maximum_val if X_test[t]-value > 0 else X_test[t] + maximum_val

        predict.append(value)
        #print(predict[t],X_test[t])
    pyplot.plot(X_test, color='red')
    pyplot.plot(predict, color='blue')
    pyplot.title("Linear estimation")
    pyplot.show()
    return predict



