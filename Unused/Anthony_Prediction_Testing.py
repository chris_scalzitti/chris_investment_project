from alpha_vantage.timeseries import TimeSeries
from alpha_vantage.techindicators import TechIndicators
from scipy.ndimage.interpolation import shift
from sklearn.metrics import mean_squared_error
import math
import numpy as np
import matplotlib.pyplot as plt
import GeneticAlgo as IM
import ARIMA

def plotPrediction(model,predicted,actual):

    plt.plot(actual, color='red', label='Actual Stock Price')
    plt.plot(predicted, color='blue', label='Predicted Stock Price')

    plt.title(model+' Stock Price Prediction')
    plt.xlabel('Time')
    plt.ylabel('Stock Price')
    plt.legend()
    plt.show()


def saveDailyData(ticker,size):

    # Your key here
    key = 'NR0TTJWDRGF1P8'
    ts = TimeSeries(key)
    ti = TechIndicators(key)
    data, meta = ts.get_daily(ticker, size)
    rsi, meta = ti.get_rsi(ticker,interval='daily',time_period='20',series_type='close')
    stoch, meta = ti.get_stoch(ticker, interval='daily')

    merged = []
    for day in data:
        combined = list(data[day].values())+ \
        list(rsi[day].values() if day in rsi else ['0']) + \
        list(stoch[day].values() if day in stoch else ['0','0'])
        merged.append(combined)

    merged = np.asarray(merged,dtype=np.float32)
    # Reverse data set
    merged = merged[::-1]
    np.savetxt("Daily_"+ticker.replace('TSX:','')+".csv", merged, delimiter=",")


def investModel(predict,actual):
    np.squeeze(predict)
    np.squeeze(actual)
    actual = actual[~np.isnan(actual)]

    # for i in range(len(actual)-1):
    #     diff = abs(predict[i]-actual[i])
    #     plt.plot(diff,diff, "g*")
    # plt.show()
    money = 50
    stocks = 0
    bankroll = []
    numofstocks = []
    for i in range (1,len(actual)):
        # stock going down sell
        #print(actual[i-1],predict[i])
        if actual[i-1] > predict[i]:
            stocks = int(stocks*.5)
            money = (stocks * actual[i-1])+money
        # stock going up buy
        if actual[i - 1] < predict[i]:
            new_stocks = math.floor(money*.5/actual[i - 1])
            stocks = stocks + new_stocks
            money = money-(new_stocks * actual[i-1])
        bankroll.append(money)
        numofstocks.append(stocks)

    final = bankroll[-1] + numofstocks[-1]*actual[-1]
    bankroll.append(final)
    print("Final money value: " + str(final))
    plotPrediction("Money",bankroll,[])
    return bankroll



def trainSVRModel(data,forcast=1,backtest=365):
    # https://towardsdatascience.com/walking-through-support-vector-regression-and-lstms-with-stock-price-prediction-45e11b620650
    from sklearn.svm import SVR

    close_index = 3
    # split and reshape X and Y data
    X, Y = data[:,close_index], data[:,close_index]
    Y = shift(Y, -forcast, cval=np.NaN)
    X = np.reshape(X, (-1, 1))

    # Test and Train
    # val = math.floor(.7 * data.shape[0])
    X_train = X[:-backtest]
    Y_train = Y[:-backtest]
    X_test = X[-backtest:]
    Y_test = Y[-backtest:]

    svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)
    svr_rbf.fit(X_train, Y_train)
    predictions = np.reshape(svr_rbf.predict(X_test),(-1,1))

    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(predictions[:-forcast], Y_test[:-forcast]))
    print('SVR RMSE: %.3f' % rmse)
    plotPrediction("SVR",predictions,Y_test)
    return (predictions,np.reshape(Y_test,(-1,1)))

def train_NN_model(actualtrain,actualtest,predicttrain,predicttest):
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import Dropout
    from matplotlib import pyplot

    model = Sequential()
    model.add(Dense(4, input_dim=predicttrain.shape[1], kernel_initializer='normal', activation='relu'))
    model.add(Dense(3, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam')
    model.fit(predicttrain, actualtrain, batch_size=1, epochs=10)


    Y_pred = model.predict(predicttest)
    pyplot.plot(predicttest, color='cyan')
    pyplot.plot(actualtest, color='red')
    pyplot.plot(Y_pred, color='blue')
    pyplot.title("NN Combination model")
    pyplot.show()

    return Y_pred


def trainLSTMModel(data,time_steps=14,forcast=1,backtest=365):
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import LSTM
    from keras.layers import Dropout
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.svm import SVR
    close_index = 3
    epochs =2
    batch =32

    # Scale Data
    X_scaler = MinMaxScaler(feature_range=(0, 1))
    Y_scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = X_scaler.fit_transform(data)
    Y_scaler.min_,Y_scaler.scale_ = X_scaler.min_[close_index],X_scaler.scale_[close_index]

    # Prepare data for LSTM
    X = []
    Y = []
    for i in range(time_steps, len(scaled_data)):
        X.append(scaled_data[i-time_steps:i,:])
        Y.append(scaled_data[i, close_index])

    X, Y = np.array(X), np.array(Y)
    Y = shift(Y, -forcast, cval=np.NaN)
    # dev = math.floor(.7 * scaled_data.shape[0])
    X_train = X[:-backtest]
    Y_train = Y[:-backtest]
    X_test = X[-backtest:]
    Y_test = Y[-backtest:]


    regressor = Sequential()

    regressor.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1],X_train.shape[2])))
    regressor.add(Dropout(0.2))

    regressor.add(LSTM(units=50, return_sequences=True))
    regressor.add(Dropout(0.2))

    regressor.add(LSTM(units=50, return_sequences=True))
    regressor.add(Dropout(0.2))

    regressor.add(LSTM(units=50))
    regressor.add(Dropout(0.2))

    regressor.add(Dense(units=1))

    regressor.compile(optimizer='adam', loss='mean_squared_error')

    regressor.fit(X_train, Y_train, epochs=epochs, batch_size=batch)
    print("Training Finished ")

    # Inverse transform and reshape
    predictions = regressor.predict(X_test)
    predictions = Y_scaler.inverse_transform(predictions)
    Y_test = np.reshape(Y_test,(-1,1))
    Y_test = Y_scaler.inverse_transform(Y_test)

    # calculate RMSE
    rmse = math.sqrt(mean_squared_error(predictions[:-forcast], Y_test[:-forcast]))
    print('LSTM RMSE: %.3f' % rmse)

    # plot prediction
    # geneticOptimizer(predicted_stock_price,Y_test)

    plotPrediction("LSTM",predictions,Y_test)
    return predictions,Y_test

# Main
# Saving TSX Stock Data
ticker = 'TSX:K'
#saveDailyData(ticker,'full')

#Open get data from file you want to model
filename = "Daily_"+ticker.replace('TSX:','')+".csv"
data = np.genfromtxt(filename, delimiter=',')

# Number of days to backtest
backtest = 365
svr_1, test_data = trainSVRModel(data,forcast=1,backtest=backtest)
# svr_2, _ = trainSVRModel(data,forcast=2,backtest=backtest)
# svr_3, _ = trainSVRModel(data,forcast=3,backtest=backtest)
lstm_1,test_data = trainLSTMModel(data,time_steps=60,forcast=1,backtest=backtest)
Arima_predict = ARIMA.train_Arima(data,backtest)
line = ARIMA.line_predict(data,backtest)
predictions1 = np.reshape(np.average(np.concatenate((lstm_1,line, svr_1,Arima_predict,),axis=1),axis=1),(-1,1))
predictions2 = np.reshape(np.average(np.concatenate((svr_1,line, Arima_predict),axis=1),axis=1),(-1,1))
predictions3 = np.reshape(np.average(np.concatenate((lstm_1,line, Arima_predict, lstm_1),axis=1),axis=1),(-1,1))
predictions4 = np.reshape(np.average(np.concatenate((lstm_1,line, line),axis=1),axis=1),(-1,1))
predictions5 = np.reshape(np.average(np.concatenate((lstm_1,line, Arima_predict),axis=1),axis=1),(-1,1))


plt.plot(predictions5,"b-", label = "Prediction")
plt.plot(test_data,'r-', label = "Actual")
plt.title("Final Predictions")
plt.show()

#NN Model
size = int(len(test_data)* 0.9)
NNTrainingdata = np.concatenate((lstm_1,predictions1,predictions2,predictions3, predictions4, predictions5, line, Arima_predict),axis=1)
AcutalNNTrain, ActualNNTest = test_data[0:size], test_data[size:len(test_data)] #Actual
predicttrain, predicttest = NNTrainingdata[0:size], NNTrainingdata[size:len(NNTrainingdata)] #prediction

NNpredict = train_NN_model(AcutalNNTrain, ActualNNTest, predicttrain, predicttest)

print("NN Model")
investModel(NNpredict,ActualNNTest)
print("SVR")
investModel(svr_1[size:len(test_data)],ActualNNTest)
print("ARIMA")
investModel(Arima_predict[size:len(test_data)],ActualNNTest)
print("LSTM")
investModel(lstm_1[size:len(test_data)],ActualNNTest)
print("Linear")
investModel(line[size:len(test_data)],ActualNNTest)
print("Regressive Averaged models")
investModel(predictions2[size:len(test_data)],ActualNNTest)
print("Averaged Predicted")
investModel(predictions1[size:len(test_data)],ActualNNTest)
investModel(predictions3[size:len(test_data)],ActualNNTest)
print("LSTM Line and ARIMA")
investModel(predictions4[size:len(test_data)],ActualNNTest)
investModel(predictions5[size:len(test_data)],ActualNNTest)

























# Avg_weight, best_weights = IM.geneticOptimizer(Genpredicttrain,GentrainActual)
# IM.BackTest(Genbacktestpredict,GenBacktestActual,Avg_weight, best_weights)
#
# Avg_weight, best_weights = IM.geneticOptimizer(predictions1,test_data)
# IM.BackTest(lstm_1,test_data,Avg_weight, best_weights)


#
# #genetic SVR
# Avg_weight, best_weights = IM.geneticOptimizer(predictions,test_data)
# IM.BackTest(predictions,test_data,Avg_weight, best_weights)

#genetic combined
# Avg_weight, best_weights = IM.geneticOptimizer2(lstm_1, svr_1,test_data)
#IM.BackTest(predictions,test_data,Avg_weight, best_weights)










