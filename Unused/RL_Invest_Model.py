import numpy as np
import matplotlib.pyplot as plt
num_actions = 12
def getReward(action,stocks,money, today_price,tomorrow_price):
    import math
    #dealing with action indexs so you have -1 from num actions/2
    percent_bucket = 1/((num_actions/2) -1)
    buy_sell_seperator_index = num_actions/2


    today_stocks = stocks
    today_money = money
    today_worth = (today_stocks * today_price) + today_money

    if action < buy_sell_seperator_index :

        invest_percent  = action *percent_bucket

        buy_money = invest_percent * money
        bought_stocks = math.floor(buy_money / today_price)
        money -= bought_stocks*today_price
        stocks += bought_stocks


    elif action > buy_sell_seperator_index:
            action = action - buy_sell_seperator_index
            divest_percent = action * percent_bucket

            sell_money = divest_percent * (today_stocks *today_price)
            sold_stocks = round(sell_money / today_price)
            money += sold_stocks * today_price
            stocks -= sold_stocks

    tomorrow_worth = (tomorrow_price*stocks) + money
    return tomorrow_worth-today_worth,stocks,money


def trainDeepQ(num_episodes, duration, prediction, prices):
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import InputLayer

    model = Sequential()
    model.add(InputLayer(batch_input_shape=(1,1)))
    model.add(Dense(2*num_actions, activation='sigmoid'))
    model.add(Dense(num_actions, activation='linear'))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])

    # now execute the q learning
    price_index =1
    y = 0.95
    eps = 0.5
    decay_factor = 0.999
    r_avg_list = [0]


    # duration = game size or number of days you want to be in the market for
    prices = np.reshape(np.squeeze(prices), (-1, duration))
    prediction = np.reshape(np.squeeze(prediction), (-1, duration))

    for i in range(num_episodes):
        stocks = 0
        money = 50
        r_sum = 0
        eps *= decay_factor
        batch = np.random.randint(0,100,1)%prices.shape[0]
        batch_pred = np.squeeze(prediction[batch])
        batch_price = np.squeeze(prices[batch])

        if i % 10 == 0:
            print("Episode {} of {}".format(i + 1, num_episodes))
            print("Return Average: "+str(r_avg_list[-1]))

        for i in range(1,len(batch_price)-1):

            state = np.reshape([batch_pred[i]-batch_price[i - 1]],(1,1))
            new_state = np.reshape([batch_pred[i+1]-batch_price[i]],(1,1))

            if np.random.random() < eps:
                action = np.random.randint(0,num_actions,1)
            else:
                action = np.argmax(model.predict(state))
            action = int(action)
            r,stocks,money = getReward(action,stocks,money,batch_price[i],batch_price[i+1])
            target= r + y * np.max(model.predict(new_state))
            target_vec = model.predict(state)[0]
            target_vec[action] = target
            model.fit(state, target_vec.reshape(-1,num_actions), epochs=1, verbose=0)
            r_sum += r

        r_avg_list.append(r_sum/(len(batch_price)-1))

    plt.plot(r_avg_list, color='blue', label='Returns Avg Each Episode')
    plt.title('Returns Avg Each Episode')
    plt.xlabel('Time')
    plt.ylabel('Returns')
    plt.legend()
    plt.show()