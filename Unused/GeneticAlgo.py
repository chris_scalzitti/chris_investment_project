import math
import random
import numpy as np
import copy


import matplotlib.pyplot as plt


class GeneticPlayer:
    bankroll = 50
    def __init__(self, mylist, bankroll= bankroll, stonk_price = 5, stonks_held = 0, candidate_number = -1, dec = []):
        self.weights = mylist
        self.bankroll = bankroll
        self.stocks_held = stonks_held
        self.price_per_stonk = stonk_price
        self.candiate_number = candidate_number
        self.dec = dec

    def __copy__(self):
        #print("Copy")
        return GeneticPlayer(self.weights.copy(), bankroll= self.bankroll, candidate_number=self.candiate_number, stonks_held=self.stocks_held, dec= self.dec)

    def tanH(self, val):
        return math.tanh(val)

    def sigmoid(self,val):
        return 1 / (1 + math.exp(round(-val,4)))

    def scaler_function(self, val, min, max): #scales between 0 and 1
        val = (val - min)/(max-min)
        return val

    def evaluate(self,predicted, current, lowest_diff, highest_diff): #making a NN structure
        if self.bankroll <= 0:
            return
        diff = predicted-current
        diff = self.scaler_function(diff,lowest_diff,highest_diff) if diff > 0 else -self.scaler_function(abs(diff),lowest_diff,highest_diff)
        val = self.tanH( diff*self.weights[0] + (self.weights[1]*self.weights[2]))
        if val > .33:
            percentage = self.sigmoid(abs(diff) * self.weights[3] + (self.weights[4]*self.weights[5]))
            percentage = 1 if percentage > .93 else percentage
            investamount = self.bankroll *percentage
            stonks_to_buy = math.floor(investamount / current)

            self.stocks_held += stonks_to_buy
            #print("Buying " +str(self.stocks_held))
            self.bankroll = self.bankroll - (stonks_to_buy*current)

            #self.dec.append(("B",diff, stonks_to_buy))
            return ("Buy", stonks_to_buy)

        elif val > -.33 and val <= .33:
            self.bankroll -= .01 if self.bankroll > 10 else 0
            #self.dec.append(("N", diff, self.stocks_held))
            return ("N",0)
        else:
            percentage = self.sigmoid(abs(diff) * self.weights[6] + (self.weights[7]) * self.weights[8])
            percentage = 1 if percentage > .93 else percentage
            divestmentamount = round(self.stocks_held * percentage)
            stonks_to_sell = divestmentamount
            self.stocks_held -= stonks_to_sell
            self.bankroll = self.bankroll + (stonks_to_sell * current)


            #self.dec.append(("S", diff, stonks_to_sell))
            return ("S", stonks_to_sell)

    def evaluate2(self, LSTM, SVR, current, lowest_diff, highest_diff, lowestdiff2, highestdiff2): #making a NN structure
        if self.bankroll <= 0:
            return
        diff = LSTM-current
        diff2 = SVR-current
        diff = self.scaler_function(diff,lowest_diff,highest_diff) if diff > 0 else -self.scaler_function(abs(diff),lowest_diff,highest_diff)
        diff2 = self.scaler_function(diff2, lowestdiff2, highestdiff2) if diff2 > 0 else -self.scaler_function(abs(diff2),lowestdiff2, highestdiff2)
        val = self.tanH((diff*self.weights[0]))#self.tanH(diff*self.weights[0]) + self.tanH(diff2*self.weights[1]) + self.tanH(self.bankroll *self.weights[2]) + self.tanH(self.stocks_held*self.weights[3]))
        if val > .33:
            percentage = self.sigmoid(abs(diff) * self.weights[4] + (abs(diff2)*self.weights[5]))
            percentage = 1 if percentage > .93 else percentage
            investamount = self.bankroll *percentage
            stonks_to_buy = math.floor(investamount / current)

            self.stocks_held += stonks_to_buy
            #print("Buying " +str(self.stocks_held))
            self.bankroll = self.bankroll - (stonks_to_buy*current)

            #self.dec.append(("B",diff, stonks_to_buy))
            return ("Buy", stonks_to_buy)

        elif val > -.33 and val <= .33:
            self.bankroll -= .05 if self.bankroll > 10 else 0
            #self.dec.append(("N", diff, self.stocks_held))
            return ("N",0)
        else:
            percentage = self.sigmoid(abs(diff) * self.weights[6] + (abs(diff2)*self.weights[7]))
            percentage = 1 if percentage > .93 else percentage
            divestmentamount = round(self.stocks_held * percentage)
            stonks_to_sell = divestmentamount
            self.stocks_held -= stonks_to_sell
            self.bankroll = self.bankroll + (stonks_to_sell * current)
            #self.dec.append(("S", diff, stonks_to_sell))
            return ("S", stonks_to_sell)


    def __eq__(self, other):
        self.weights = other.weights.copy()
        return self

    def print(self):
        print("Candidate: " + str(self.candiate_number) + " Bankroll: " + str(round(self.bankroll,4)) + "  Stocks Held: " + str(round(self.stocks_held)) + "  weights:  " + str(self.weights))

class GeneticDecider():
    def __init__(self, sway_rate = .2, mutate = .2, invert = .2, cross = .2, learning_rate = .1,  num=3):
        self.sway_rate= sway_rate
        self.mutate_prob = mutate
        self.invert_prob = invert
        self.crossover_prob = cross
        self.num_of_weights = num
        self.learning_rate = learning_rate


    def new_Gen(self, Old_Gen, current, viz):
        for i in Old_Gen:
            i.bankroll += (current*i.stocks_held)
            i.stocks_held = 0
        sorted_list = self.fitness_function(Old_Gen)
        old_g = []
        for g in sorted_list:
            old_g.append(copy.copy(g))
        viz.append(old_g)  # keep generatinos weights
        new_gen = self.create_children(sorted_list)
        return new_gen, viz

    def fitness_function(self, Player_list): #ment for siz player poker
        sorted_Placting_List = sorted(Player_list,key=fitness_sorting_key)#sorting from least chips to highest: highest is best candiate
        for i in range(len(sorted_Placting_List)):
            sorted_Placting_List[i].candiate_number = len(sorted_Placting_List) - i
        return sorted_Placting_List

    def create_children(self, sorted_list):
        Best_candidate = sorted_list[-1]
        Second_candidate = sorted_list[-2]
        Third_candidate = sorted_list[-3]
        Third_candidate = self.Sway(Best_candidate,Third_candidate)

        children = [Best_candidate,Second_candidate,Third_candidate]

        for i in range(len(sorted_list)-3):
            weighted_prob = sorted_list[i].scaler_function(sorted_list[i].candiate_number, 1, len(sorted_list)-3)
            prob = random.random()
            #print("Weighted prob: " + str(weighted_prob) + " prob : " + str(prob) + "   is prob < wieghted")
            if prob < weighted_prob:
                ran = 2*random.random()-1
                if ran >= .33: #mutate
                    sorted_list[i] = self.Mutate(sorted_list[i])

                elif ran < .33 and ran > -.33:  #crossOver
                    sorted_list[i] = self.cross_over(Best_candidate, sorted_list[i])

                else: #invert
                    sorted_list[i] = self.Invert(sorted_list[i])

            children.append(sorted_list[i])

        new_sorted_children = sorted(children,key=candiate_sorting_key)
        return new_sorted_children

    def Sway(self,BestPlayer,GenPlayer):
        for i in range(len(GenPlayer.weights)):
            ran = random.random()
            ran = 1 if ran >.5 else -1
            GenPlayer.weights[i] = ran*self.learning_rate*BestPlayer.weights[i] + BestPlayer.weights[i]
        return GenPlayer

    def Mutate(self, GenPlayer):
        for i in range(len(GenPlayer.weights)):
            ran = random.random()
            if ran >= (1-self.mutate_prob):
                #print("inside mutate")
                ran = 4 *random.random() -2
                GenPlayer.weights[i] = ran

        return GenPlayer

    def Invert(self, GenPlayer):
        for i in range(len(GenPlayer.weights)):
            ran = random.random()
            if ran >= (1-self.invert_prob):
                #print("inside invert")
                GenPlayer.weights[i] = -GenPlayer.weights[i]

        return GenPlayer

    def cross_over(self, BestPlayer, GenPlayer):
        for i in range(len(GenPlayer.weights)-1):
            ran = random.random()
            if ran >= (1-self.crossover_prob):
                #print("inside of corssover")
                GenPlayer.weights[i] = BestPlayer.weights[i]
        return GenPlayer

    def create_random_n_candiates(self, num):
        candidate_list = []
        random_weights = []

        for i in range(num):
            for j in range(self.num_of_weights):
                ran = 4*random.random()-2
                random_weights.append(ran)
            candidate_list.append(GeneticPlayer(random_weights))
            random_weights = []

        return candidate_list


def fitness_sorting_key(entity):
    return entity.bankroll

def candiate_sorting_key(entity):
    entity.bankroll = GeneticPlayer.bankroll#============================================Change This Bankroll
    return entity.candiate_number


def Plot_Genes(vizualization):
    plotter = [[] for i in range(len(vizualization[0]))]
    best = []
    second = []
    third = []
    fourth = []
    for i in range(len(vizualization)):
        for j in range(len(vizualization[0])): #nuber of creatures
            plotter[j].append(vizualization[i][j])
            if vizualization[i][j].candiate_number == 1:
                best.append(vizualization[i][j])
            if vizualization[i][j].candiate_number == 2:
                second.append(vizualization[i][j])
            if vizualization[i][j].candiate_number == 3:
                third.append(vizualization[i][j])
            if vizualization[i][j].candiate_number == 4:
                fourth.append(vizualization[i][j])

    for a in plotter:#for each candidate
        for b in a: #print all of n candidates
            plt.plot(b.candiate_number,b.bankroll,"ro")
    plt.title('Bankroll')
    plt.xlabel('Candidate Number')
    plt.ylabel('Money')
    plt.show()


    for i in range(len(best)):
        plt.plot(i, fourth[i].bankroll, 'go')
        plt.plot(i, third[i].bankroll, 'yo')
        plt.plot(i, second[i].bankroll, 'bo')
        plt.plot(i, best[i].bankroll, 'ro')
    plt.title('Bankroll')
    plt.xlabel('Iteration')
    plt.ylabel('Money')
    plt.show()

    best_bankroll = 0
    best_weights = []
    AVG_Weights = []
    for i in range(len(best[0].weights)):
        sum = 0
        for j in range(10,len(best)):
            #print("bankroll: " + str(best[j].bankroll))
            if best[j].bankroll > best_bankroll:
                best_bankroll = best[j].bankroll
                best_weights = best[j].weights.copy()

            sum += best[j].weights[i]

        Avg = sum/len(best)
        AVG_Weights.append(Avg)

    # Uncomment to see if weights converge
    # for i in range(len(best[0].weights)):
    #     for j in range(len(best)):
    #         plt.plot(j, fourth[j].weights[i], 'go')
    #         plt.plot(j, third[j].weights[i], 'yo')
    #         plt.plot(j, second[j].weights[i], 'bo')
    #         plt.plot(j, best[j].weights[i], 'ro')
    #     plt.title("Weight " + str(i))
    #     plt.xlabel('Iteration')
    #     plt.ylabel('Value')
    #     plt.show()

    print("Bankroll: " + str(best_bankroll))
    return AVG_Weights, best_weights


def get_min_difference(predicted, actual):
    min_diff = 9999
    for i in range(1,len(actual)):
        diff =  float(abs(predicted[i] - actual[i]))
        if diff < min_diff:
            min_diff = diff
    return min_diff

def get_max_difference(predicted,actual):
    max_diff = -1
    for i in range(1,len(actual)):
        diff = float(abs(predicted[i] - actual[i]))
        if diff > max_diff:
            max_diff = diff
    return max_diff

def geneticOptimizer(predicted,actual):
    #--------Genetic---------------
    number_of_iterations = 150  # num of iterations
    GD= GeneticDecider(.9,.9,.9,num=9)
    new_gen = GD.create_random_n_candiates(50)
    visualization = [] #storing all the generation weights
    lowest = get_min_difference(predicted,actual)
    highest = get_max_difference(predicted,actual)
    #--------------------------------------


    for i in range(number_of_iterations):
        prob = new_gen[0].scaler_function(number_of_iterations-i-1,0,number_of_iterations-1)
        GD = GeneticDecider(prob,prob,prob,num=9)
        print("Iteration: " + str(i))
        for d in range(len(actual)-1):
            for j in range(len(new_gen)):
                new_gen[j].evaluate(round(float(predicted[d+1]),4), round(float(actual[d]),4), float(lowest), float(highest))
        #print(new_gen[95].dec)
        if i == number_of_iterations-1: #do not allow for a new generation on last iteration
            break

        #-----Overload Copy Function to avoid shallow copy
        old_gen = []
        for g in new_gen:
            old_gen.append(copy.copy(g))
        #------------------------------------
        new_gen, visualization = GD.new_Gen(old_gen,float(actual[-2]), visualization) #create the new generation

    # for i in range(len(visualization)): #final prints of the genetic algo
    #     for j in range(len(new_gen)):
    #         visualization[i][j].print()
    #     print("\n")

    AVGW, bestW = Plot_Genes(visualization)
    return AVGW, bestW
#---------------------------------------------------------------------------------------------
def geneticOptimizer2(LSTM, SVR,actual):
    #--------Genetic---------------
    number_of_iterations = 150  # num of iterations
    GD= GeneticDecider(.9,.9,.9,num=8)
    new_gen = GD.create_random_n_candiates(50)
    visualization = [] #storing all the generation weights
    lowestLSTM = get_min_difference(LSTM,actual)
    highestLSTM = get_max_difference(LSTM,actual)
    lowestSVR = get_min_difference(SVR,actual)
    highestSVR = get_max_difference(SVR,actual)
    #--------------------------------------


    for i in range(number_of_iterations):
        prob = new_gen[0].scaler_function(number_of_iterations-i-1,0,number_of_iterations-1)
        GD = GeneticDecider(prob,prob,prob,num=8)
        print("Iteration: " + str(i))
        for d in range(len(actual)-1):
            for j in range(len(new_gen)):
                new_gen[j].evaluate2(round(float(LSTM[d+1]),4),round(float(SVR[d+1]),4), round(float(actual[d]),4), float(lowestLSTM), float(highestLSTM), float(lowestSVR), float(highestSVR))

        #print(new_gen[95].dec)
        if i == number_of_iterations-1: #do not allow for a new generation on last iteration
            break

        #-----Overload Copy Function to avoid shallow copy
        old_gen = []
        for g in new_gen:
            old_gen.append(copy.copy(g))
        #------------------------------------
        new_gen, visualization = GD.new_Gen(old_gen,float(actual[-2]), visualization) #create the new generation

    # for i in range(len(visualization)): #final prints of the genetic algo
    #     for j in range(len(new_gen)):
    #         visualization[i][j].print()
    #     print("\n")

    AVGW, bestW = Plot_Genes(visualization)
    return AVGW, bestW

#=======================================================================================
#=== Back Testing

def BackTest (predicted,actual,AVG_weights, best_weights):
    lowest = get_min_difference(predicted, actual)
    highest = get_max_difference(predicted, actual)
    #print("Weights: " + str(AVG_weights))
    AVGbot = GeneticPlayer(AVG_weights, bankroll=GeneticPlayer.bankroll,  stonks_held = 0, candidate_number = -1)
    Bestbot = GeneticPlayer(best_weights, bankroll=GeneticPlayer.bankroll,  stonks_held = 0, candidate_number = -1)
    for p in range(len(actual)-1):
        plt.plot(p, AVGbot.bankroll, "g*")
        plt.plot(p,Bestbot.bankroll, "y*")
        AVGbot.evaluate(round(float(predicted[p + 1]), 4), round(float(actual[p]), 4), float(lowest),float(highest))
        Bestbot.evaluate(round(float(predicted[p + 1]), 4), round(float(actual[p]), 4), float(lowest),float(highest))

    AVGbot.bankroll += AVGbot.stocks_held*float(actual[-2])
    AVGbot.stocks_held = 0
    Bestbot.bankroll += Bestbot.stocks_held * float(actual[-2])
    Bestbot.stocks_held = 0
    plt.plot(len(actual), AVGbot.bankroll, "bo")
    plt.plot(len(actual), Bestbot.bankroll, "ro")
    print("AVGBot Bank Roll: " + str(AVGbot.bankroll))
    print("BestBot Bank Roll: " + str(Bestbot.bankroll))
    plt.show()

def get_decsision(predicted,actual,AVG_weights, best_weights):
    lowest = get_min_difference(predicted, actual)
    highest = get_max_difference(predicted, actual)
    AVG_Move = []
    Best_Move = []
    #print("Weights: " + str(AVG_weights))
    AVGbot = GeneticPlayer(AVG_weights, bankroll=GeneticPlayer.bankroll,  stonks_held = 0, candidate_number = -1)
    Bestbot = GeneticPlayer(best_weights, bankroll=GeneticPlayer.bankroll,  stonks_held = 0, candidate_number = -1)
    for p in range(len(actual)-1):
        plt.plot(p, AVGbot.bankroll, "g*")
        plt.plot(p,Bestbot.bankroll, "y*")
        AVG_Move.append(AVGbot.evaluate(round(float(predicted[p + 1]), 4), round(float(actual[p]), 4), float(lowest),float(highest)))
        Best_Move.append(Bestbot.evaluate(round(float(predicted[p + 1]), 4), round(float(actual[p]), 4), float(lowest),float(highest)))

    return Best_Move[-1]




